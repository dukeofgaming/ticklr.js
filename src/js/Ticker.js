var Ticker	= new Class({
	Implements	: Options,
	
	options		: {
		items		: [],				// An array with TickerItem objects
		layout		: 'horizontal',		// 'horizontal', 'vertical'
		direction	: 'left',			// 'up', 'down', 'left', 'right'
		size		: 5,				// The amount of visible items at a time
		interval	: 1000,				// Amount of time to wait on each new scrolled element, use 0 for fluid
		speed		: 1000,				// Time at which each new element is shown
		rewind		: false,			// If false, elements disappearing will be put at the end of the queue
	},
	
	initialize	: function(options){
		this.setOptions(options);
	},
	
	start		: function(){
		
	},
	
	stop		: function(){
		
	},
	
	previous	: function(){
		
	},
	
	next		: function(){
		
	},
	
	rewind		: function(){
		
	}	
});