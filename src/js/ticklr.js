
var Ticklr  =   new Class({
	Implements	: Options,
	options		: {
		title		: null,				// null, 'text' or $(element)
		data		: [],				// an array with plain data objects representing each item, or a URL to load items by ajax
		controls	: {
			buttons	: {
				previous	: false,
				pause		: false,
				next		: false				
			},
			container	: null			// null or element
		},
		
		autoreload	: false				// If true and data is a URL, 
	},
	initialize	: function(options){
		this.setOptions(options);
	}
	
});

