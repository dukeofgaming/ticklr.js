var TickerItem	= new Class({
	Implements	: Options,
	
	options		: {
		icon		: '',
		title		: '',
		subtitle	: '',
		summary		: ''
	},
	
	initialize	: function(options){
		this.setOptions(options);
	}
});